//container for all displayed cases
var displayed = [];
//container for all temporary (hover) data
var tempDisplayed = [];
//container for all all permanent data points that are also being showed as cited
var displayedAndCited = [];
//container for all locked temporary (hover) data
var tempLocked = [];
//container for all displayed svg paths
var svgPaths = [];
//container for all path Nodes
var newPathNodes = [];
var oldPathNodes = [];
var citations = [];
// An object where the key is an issue name and the  value is a scale.
// Each scale is a mapping (called as a function) from number of connections to the place where the case should be displayed.
var xScales = {};
var locked = false;
// Set up size
var width = 3000,
  height = width;
var format = d3.time.format('%Y');
var start = format.parse('1950');
var end = format.parse('2000');
var timeScale = d3.time.scale().domain([
  start,
  end
]).range([
  35,
  635
]);
//Add global svg to DOM
var svg = d3.select('svg').attr('width', width).attr('height', height).append('g').attr('transform', 'translate(50, 0)');
//class 1 group
// var class1 = svg.append("g")
// 				
//Y axis
var yAxis = d3.svg.axis().scale(timeScale) // .ticks(10)
  .tickSize(-width, 0, 0).orient('left').tickPadding(10).tickFormat(d3.time.format('%Y'));
//append y-axis group that spans width of svg
svg.append('g').attr('transform', 'translate(20, 0)').attr('class', 'grid').call(yAxis);
//create addIssue frame
var addIssue = svg.append('rect').attr('x', 30).attr('y', 35).attr('width', 400).attr('height', 600).attr('stroke', '#3498db').attr('stroke-width', 2).attr('fill', 'white').attr('stroke-dasharray', '10,10').attr('d', 'M5 40 l215 0').attr('class', 'addIssue').on('click', function () {
  $('#issueMod').openModal(); // createPane();
});

String.prototype.cameBefore = function (otherCite) {
  var comparisonRegex = /(\d+) U.S. (\d+)/
  var thisComparisonArray = comparisonRegex.exec(this);
  var otherCiteComparisonArray = comparisonRegex.exec(otherCite);
  thisComparisonArray = thisComparisonArray.map(function (numString) {
    return parseInt(numString, 10)
  })
  otherCiteComparisonArray = otherCiteComparisonArray.map(function (numString) {
    return parseInt(numString, 10)
  })
  // Since cases are formatted # U.S. #, we first compare the first numbers, then if they are equal, compare the second numbers.
  if (thisComparisonArray[1] < otherCiteComparisonArray[1]) {
    return true;
  } else if (thisComparisonArray[1] === otherCiteComparisonArray[1] && thisComparisonArray[2] < otherCiteComparisonArray[2]) {
    return true;
  } else {
    return false;
  }
}

function connectionsToPreAndPostConnections() { // This is an unnecessary step given our current data, but it's just to be safe so we don't need to check if a connection came before later on where it's more expensive.
  d3.json('data/batch5.json', function (error, data) {
    data.map(function (cite) {
      cite.postConnections = []
      cite.connections = cite.connections.filter(function (connection) {
        if (connection.cameBefore(cite.usCite)) {
          return true; // For now I will be leaving the connections that come before in the connections array.
        } else {
          cite.postConnections.push(connection);
          return false;
        }
      });
    });
    var blob = new Blob([JSON.stringify(data)], {type : 'text/json'});
    var url = URL.createObjectURL(blob);
    window.open(url, '_blank');
    window.focus();
  })
}


//FUNCTION: reads legal.json array into datarray variable
var getData = function () {
  d3.json('data/batch5.json', function (error, data) {
    dataArray = data; // dataArray
    var usCiteArray = data.map(function (cite) {
      return cite.usCite;
    });
    var usCiteData = usCiteArray.reduce(function (pre, cur, index) {
      // materializecss's autocomplete takes an object with keys being the actual text used for completion and the values being links to pictures, since there are no pictures, I just add keys with null values.
      pre["" + cur] = null;
      return pre;
    }, {});
    $('input.autocomplete').autocomplete({ //autocomplete comes from the new materialize.js
      data: usCiteData
    });
  });
};
getData();
//FUNCTION: controls the drag and drop of frames
function onDragDrop() {
  var drag = d3.behavior.drag();
  drag.on('drag', dragmove).on('dragend', dropHandler);
  return drag;
}
//FUNCTION: remove all locked cases
function clearLocked() {
  d3.selectAll('circle.locked').remove();
  d3.selectAll('circle.temp').remove();
  lockParent.attr('stroke-width', 0)
  tempLocked = [];
  locked = false;
}
//FUNCTION: removes all svg paths and extra path nodes, de-highlights old path nodes.
function clearPaths() {
  var i = null;
  for (i = 0; i < svgPaths.length; i++) {
    d3.select('[id=\'' + svgPaths[i] + '\']').remove();
  }
  for (i = 0; i < newPathNodes.length; i++) {
    d3.select('[name=\'' + newPathNodes[i] + '\']').remove();
    displayed = _.without(displayed, newPathNodes[i]);
    tempLocked = _.without(tempLocked, newPathNodes[i]);
  }
  for (i = 0; i < oldPathNodes.length; i++) {
    data = d3.select('[name=\'' + oldPathNodes[i] + '\']');
    if (data.attr('name') == lockParent.attr('name')) {
      data.attr('stroke-width', 0);
    } else if (_.contains(citations, data.attr('name'))) {
      data.attr('stroke', 'black').attr('stroke-width', '2');
    } else {
      data.attr('stroke', data.attr('fill'));
    }
  }
  svgPaths = [];
  newPathNodes = [];
  oldPathNodes = [];
}
//FUNCTION: move frame horizontally on drag
function dragmove(d) {
  d.x += d3.event.dx;
  d3.select(this).attr('transform', 'translate(' + d.x + ',' + d.y + ')');
}
//FUNCTION: drop handler (empty)
function dropHandler(d) {}
//FUNCTION: dismisses toast on mouseout of data
function dismissToast() {
  $(document).on('click', '#toast-container .toast', function () {
    $(this).fadeOut(function () {
      $(this).remove();
    });
  });
}