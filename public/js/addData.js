////////////////////////////////////////////////////////
// addData.js:
// -----------
// All functions called when a data element needs to be 
// added to the display. Event handlers for data elemnts
// included.
////////////////////////////////////////////////////////
//FUNCTION: add data point into specific issueFrame
//CALLED ON: Modal submit
var addData = function () {
  var usCite = $('#case_id').val();
  $('#case_id').val('');
  var Case = _.find(dataArray, function (c) {
    return c.usCite == usCite;
  });
  if (Case == undefined) {
    alert('Could not find case. Please enter a new one');
    $('#case_name').val('');
    return;
  }
  var notes = $('#case_notes').val();
  $('#case_notes').val('');
  var year = format.parse(Case.term);
  $('#caseMod').closeModal();
  //addBtn is globaly set on click of add button
  var issue = addBtn.attr('class');
  var paneIdentifier = 'svg .' + issue;
  var pane = d3.select(paneIdentifier);
  var x = Number(addBtn.attr('frameX'));
  var dataPoint = pane.append('circle').attr('cx', function () {
    return xScales[issue](Case.connections.length);
  }).attr('cy', function () {
    return timeScale(year);
  }).attr('frameX', x).attr('r', 9).attr('fill', addBtn.attr('stroke')).attr('fillCopy', addBtn.attr('stroke')).attr('name', usCite).attr('issue', issue).on('mouseover', function () {
    onHover(usCite, d3.select(this), Case, notes);
  }).on('mouseout', function () {
    onMouseOut(d3.select(this));
  }).on('click', function () {
    onClick(d3.select(this));
  });

  displayed.push(usCite);
};

function onClick(data) {
  //if not in locked state
  if (!locked) {
    lockParent = data;
    //used to determine parent for locked elements and paths
    data.attr('stroke', 'grey').attr('stroke-width', '3');
    initLockState(data);
    locked = true;
  } //if in locked state
  else {
    //if clicked node is the lock parent, exit locked state with clearLocked
    if (data.attr('name') === lockParent.attr('name')) {
      console.log('clicked on locked parent');
      data.attr('stroke', data.attr('fill')).attr('stroke-width', '1');
      clearLocked();
      parentNode = data;
      displayCited(); //if not the locked parent, display paths
    } else {
      console.log('about to display paths');
      targetNode = data;
      //used to set target when drawing paths
      displayPaths();
    }
    locked = false;
  }
}

function onHover(usCite, data, Case, notes) {
  //run toast, call displayCited
  Materialize.toast(Case.caseName + '<br>' + usCite + '<br>' + notes, 12000);
  if (!locked) {
    data.attr('stroke', 'grey').attr('stroke-width', '3');
    parentNode = data;
    //used to set parent when displaying citations
    displayCited(); //loads tempDisplayed
  }
  if (locked && data.attr('name') != lockParent.attr('name')) {
    targetNode = data;
    //used to set target when drawing paths
    displayPaths();
  }
}

function onMouseOut(data) {
  //remove toast,
  $('#toast-container .toast').remove();
  if (!locked) {
    //remove temp citation elements
    var tempSelectionArgument = "circle.temp";
    d3.selectAll(tempSelectionArgument).remove()
    tempDisplayed = [];
    //revert displayed and cited elements back to just displayed
    for (var i = 0; i < displayedAndCited.length; i++) {
      d3.select('[usCite=\'' + displayedAndCited[i] + '\']').attr('stroke', data.attr('fill')).attr('stoke-width', '1');
    }
    displayedAndCited = [];
    data.attr('stroke', data.attr('fill')).attr('stroke-width', '1');
  }
  if (locked /*and not something else...*/ ) {
    clearPaths();
  }
}
//FUNCTION: add temporary citation data point into specific issueFrame
//CALLED BY: displayCited
var addTempData = function (id) {
  var usCite = id;
  var tempCase = _.find(dataArray, function (c) {
    return c.usCite == usCite;
  });
  if (tempCase == undefined) {
    console.log('Could not find case. Please enter a new one');
    $('#case_name').val('');
    return;
  }
  //  tempDisplayed.push(tempCase);
  var year = format.parse(tempCase.term);
  //parentNode is globaly set in onHover
  var issue = parentNode.attr('issue');
  var x = Number(parentNode.attr('frameX'));
  var paneIdentifier = 'svg .' + issue; // What is the g? I guess it's the pane
  var pane = d3.select(paneIdentifier);
  var dataPoint = pane.append('circle').attr('cx', function () {
      return xScales[issue](tempCase.connections.length);
    }).attr('cy', function () {
      return timeScale(year);
    }).attr('r', 9)
    .classed('temp', true).attr('fill', 'none').attr('stroke-width', '2').attr('stroke', 'gray'); // Coding convetion: classed following by what makes up that class.
  tempDisplayed.push(usCite);
};

function updateTemp() {

}
//FUNCTION: add locked data point into specific issueFrame.
//CALLED ON: data/case click
var addLockedData = function (id) {
  var usCite = id;
  var Case = _.find(dataArray, function (c) {
    return c.usCite == usCite;
  });
  if (Case == undefined) {
    console.log('Could not find case. Please enter a new one');
    $('#case_name').val('');
    return;
  }
  var year = format.parse(Case.term);
  //lockParent is global onClick
  var issue = lockParent.attr('issue');
  console.log('issue: ' + issue);
  var x = Number(lockParent.attr('frameX'));
  var paneIdentifier = 'svg .' + issue;
  var pane = d3.select(paneIdentifier);
  pane.append('circle')
    .attr('cx', function () {
      return xScales[issue](Case.connections.length);
    })
    .attr('cy', function () {
      return timeScale(year);
    })
    .attr('r', 9)
    .classed('locked', true).attr('stroke', 'black').attr('stroke-width', '2').attr('fill', 'white') // Specific to locked circles
    .attr('name', usCite)
    .attr('issue', issue)
    .on('mouseover', function () {
      Materialize.toast(Case.caseName + '<br>' + usCite + '<br>', 12000);
    })
    .on('mouseout', function () {
      $('#toast-container .toast').remove();
    })
    .on('click', function () {
      var usCite = d3.select(this).attr('name');
      $('#case_id').val(usCite);
      d3.select(this).remove();
      tempLocked = _.without(tempLocked, usCite);
      $('#caseMod').openModal();
    });
  tempLocked.push(usCite);
};