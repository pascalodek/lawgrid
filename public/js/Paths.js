/////////////////////////////////////////////////////////
// Paths.js:
// ---------
// Contains getPaths() and drawPaths(). 
// Both functions called by displayPaths in toolTips.js
/////////////////////////////////////////////////////////
//FUNCTION: gets all the paths from a start case to a target case
//CALLED BY: displayPaths
function getPaths(start, target) {
  var targetObject = _.find(dataArray, function (c) {
    return c.usCite == target;
  });
  var retPaths = [];
  // maintain a queue of paths
  var queue = [];
  // push the first path into the queue
  queue.push([start]);
  var neighborNames;
  var caseName;
  var caseObject;
  var pathFound = false;
  var new_path;
  var path;
  while (queue.length != 0) {
    // get the first path from the queue
    path = queue.shift();
    // get the last node from the path
    caseName = path[path.length - 1];
    pathFound = caseName === targetObject.usCite
    if (pathFound) {
      retPaths.push(path);
      return retPaths;
    } else {
      caseObject = _.findWhere(dataArray, {
        usCite: caseName
      });
      if (caseObject !== undefined) {
        neighborNames = caseObject.connections;
      } else {
        continue; // We don't have that case in our database!
      }
      for (var i = 0; i < neighborNames.length; i++) {
        new_path = path.slice(0); // make a copy of path.
        new_path.push(neighborNames[i]); // add a neighbor name to the copied path
        queue.push(new_path);
      }
    }
  }
  return retPaths;
}
//FUNCTION: draws a line from one case to another, add to path to paths array
//CALLED BY: displayPaths
function drawPath(case1, case2) {
  console.log('drawing path');
  if (!_.contains(displayed, case1) && !_.contains(tempLocked, case1)) {
    addLockedData(case1);
    newPathNodes.push(case1);
  } else {
    oldPathNodes.push(case1);
  }
  case1 = d3.select('[name=\'' + case1 + '\']').attr('stroke', 'green');
  if (!_.contains(displayed, case2) && !_.contains(tempLocked, case2)) {
    addLockedData(case2);
    newPathNodes.push(case2);
  } else {
    oldPathNodes.push(case2);
  }
  case2 = d3.select('[name=\'' + case2 + '\']').attr('stroke', 'green');
  // console.log("case1 object: "+case1+" case2 object: "+case2);
  var x1 = Number(case1.attr('cx'));
  var y1 = Number(case1.attr('cy'));
  var x2 = Number(case2.attr('cx'));
  var y2 = Number(case2.attr('cy'));
  path = svg.append('path').attr('d', 'M' + x1 + ' ' + y1 + ' ' + x2 + ' ' + y2).attr('stroke', 'black').attr('stroke-width', '1').attr('id', case1.attr('name') + case2.attr('name'));
  svgPaths.push(case1.attr('name') + case2.attr('name'));
}