//FUNCTION: creates a new issue frame on modal submit
var createPane = function () {
  issue = $('#issue_name').val();
  $('#issue_name').val('');
  color = $('#issue_color').val();
  if ($('#issue_color').val() === '') {
    color = 'black';
  }
  $('#issueMod').closeModal();
  var frameWidth = Number(addIssue.attr('width'));
  var x = Number(addIssue.attr('x'));
  var y = Number(addIssue.attr('y'));
  var d = [{
    x: 0,
    y: 0
    }];
  var g = svg.data(d).append('g').attr('class', issue).call(onDragDrop(dragmove, dropHandler));
  // .attr("transform", "translate(20, 0)")
  // .call(yAxis);
  var range = x + 400;
  var xScale = d3.scale.linear().domain([
    0,
    97
  ]).range([
    x,
    range
  ]);
  xScales[issue] = xScale;
  var xAxis = d3.svg.axis() // .ticks(10)
    .scale(xScale).orient('bottom').tickSize(-600);
  // .tickPadding(10);
  // .tickFormat(d3.time.format("%Y"));
  g.append('g').attr('transform', 'translate(0, 634)').attr('fill', 'none').attr('class', 'grid').call(xAxis);
  //add button
  // g.append("circle")
  // 	.attr("cx", x + 200)
  // 	.attr("cy", 15)
  // 	.attr("r", 10)
  // 	.attr("stroke", color)
  // 	.attr("fill", "white")
  // 	.attr("cursor", "pointer")
  // 	.attr("class", issue)
  // 	.attr("id", issue+"add")
  // 	.attr("frameX", x)
  // 	.attr("frameY", y)
  // 	.on("click", function() { //add data point on click
  // 		//addData(d3.select(this));
  // 		// $('.caseMod').attr('id', issue);
  // 		// alert($('.caseMod').attr('id'));
  // 		addBtn = d3.select(this);
  // 		$('#caseMod').openModal();
  // 	});
  g.append('text').attr('x', x + 200).attr('y', 30).attr('stroke', color).attr('fill', 'none').attr('cursor', 'pointer').attr('class', issue).text('+').attr('font-size', '36px').attr('id', issue + 'add').attr('frameX', x).attr('frameY', y).on('click', function () {
    //add data point on click
    //addData(d3.select(this));
    // $('.caseMod').attr('id', issue);
    // alert($('.caseMod').attr('id'));
    addBtn = d3.select(this);
    $('#caseMod').openModal();
  });
  //Issue frame
  g.append('rect').attr('x', x).attr('y', y).attr('width', frameWidth).attr('height', 600).attr('stroke', color).attr('stroke-width', 2).attr('fill', 'none').attr('class', issue);
  //Issue header
  g.append('text').attr('x', x).attr('y', y - 10).text(issue).attr('font-family', 'sans-serif').attr('font-size', '20px').attr('fill', color);
  //scooch addIssue over
  addIssue.transition().attr('x', x + frameWidth + 3);
};