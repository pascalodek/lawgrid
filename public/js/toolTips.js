/////////////////////////////////////////////////////////////
// toolTips.js:
// ------------
// All functions run as a result of hovering or clicking on data. 
// All calls made from .on handlers in addDatda()
/////////////////////////////////////////////////////////////
//FUNCTION: initiate lock state
function initLockState(pNode) {
  //remove temporary elements created on hover, replace with locked elements, empty temp displayed
  for (var i = 0; i < tempDisplayed.length; i++) {
    console.log('tempDisplayed element in .on click loop: ' + tempDisplayed[i]);
    d3.select('[usCite=\'' + tempDisplayed[i] + '\']').remove();
    parentNode = pNode;
    addLockedData(tempDisplayed[i]);
  }
  tempDisplayed = [];
  Materialize.toast('Citations for ' + parentNode.usCite + ' temporily locked', 3000);
}
//FUNCTION: searches for and displays cited cases on hover. takes in d3 case object
//CALLED ON: hover of data point
function displayCited(hoveredCase) {
  var usCite = parentNode.attr('name');
  var Case = _.find(dataArray, function (c) {
    return c.usCite == usCite;
  });
  citations = Case.connections;
  for (var i = 0; i < citations.length; i++) {
    if (!_.contains(displayed, citations[i]))
      addTempData(citations[i]);
    else {
      d3.select('[usCite=\'' + citations[i] + '\']').attr('stroke', 'black').attr('stroke-width', '2');
      displayedAndCited.push(citations[i]);
    }
  }
}
//FUNCTION: uses getPaths to find all paths between parent and displayed nodes
//CALLED ON: hover of data/case when state is locked
function displayPaths() {
  var parent = lockParent.attr('name');
  //globally set in onClick
  var target = targetNode.attr('name');
  //globally set in onHover
  var paths = getPaths(parent, target);
  //an array of paths from parent to target -- [[c1, c2], [c1, c3, c4]]
  for (var i = 0; i < paths.length; i++) {
    // console.log("building path " + i + " :");
    var path = paths[i];
    // console.log("nodes: "+path.length);
    for (var j = 0; j < path.length - 1; j++) {
      console.log('drawing edge between: ' + path[j] + ' and ' + path[j + 1]);
      // console.log("j: "+j+" j+1: "+j+1);
      drawPath(path[j], path[j + 1]);
    }
  }  // var gIdentifier = "svg ." + issue;
     // var g = d3.select(gIdentifier);
}